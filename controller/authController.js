"use-strict";

const async = require("async");
const models = require("../models");
const md5 = require("md5");
const jwt = require("../utils/jwt");

const login = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [
          {
            name: "email",
            value: req.body.email
          },
          {
            name: "password",
            value: req.body.password
          }
        ];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function checkUser(data, callback) {
        let query =
          "select id, email, role_id, phone, status from users where email = '" +
          req.body.email +
          "' and password = '" +
          md5(req.body.password) +
          "'";

        models.sequelize.query(query).then((result, err) => {
          console.log(result[0].length);
          if (err)
            return callback({
              error: true,
              message: err,
              data: {}
            });

          if (result[0].length < 1)
            return callback({
              error: true,
              message: "Username or password is wrong",
              data: {}
            });

          let token = jwt.generateAccessToken(req.body);
          result[0][0].token = token;

          callback(null, {
            error: false,
            code: "00",
            message: "Success Login",
            data: result[0][0]
          });
        });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

module.exports = { login };
