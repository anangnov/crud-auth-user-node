"use-strict";

const async = require("async");
const models = require("../models");
const md5 = require("md5");

const createUser = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [
          {
            name: "email",
            value: req.body.email
          },
          {
            name: "name",
            value: req.body.name
          },
          {
            name: "phone",
            value: req.body.phone
          },
          {
            name: "password",
            value: req.body.password
          },
          {
            name: "role_id",
            value: req.body.role_id
          }
        ];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function checkEmail(data, callback) {
        models.sequelize
          .query("select * from users where email = '" + req.body.email + "'")
          .then((result, err) => {
            if (err)
              return callback({
                error: true,
                message: err,
                data: {}
              });

            if (result[0].length > 0)
              return callback({
                error: true,
                message: "Email was registered",
                data: {}
              });

            callback(null, data);
          })
          .catch(err => {
            callback(null, err);
          });
      },
      function checkPhone(data, callback) {
        models.sequelize
          .query("select * from users where phone = '" + req.body.phone + "'")
          .then((result, err) => {
            if (err)
              return callback({
                error: true,
                message: err,
                data: {}
              });

            if (result[0].length > 0)
              return callback({
                error: true,
                message: "Phone was registered",
                data: {}
              });

            callback(null, data);
          })
          .catch(err => {
            callback(null, err);
          });
      },
      function createData(data, callback) {
        let request = {
          email: req.body.email,
          name: req.body.name,
          phone: req.body.phone,
          status: 1,
          password: md5(req.body.password),
          role_id: req.body.role_id
        };

        models.user.create(request).then((result, err) => {
          if (err)
            return callback({
              error: true,
              message: err,
              data: {}
            });
            
          callback(null, {
            error: false,
            code: "00",
            message: "Success Create User",
            data: {}
          });
        });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

const listUser = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function getData(data, callback) {
        let userId = req.params.user_id;
        models.user
          .findAll({
              where: {
                  role_id: 2
              }
          })
          .then((result, err) => {
            if (result[0].length < 1)
              return callback({
                error: true,
                message: "Not Found",
                data: {}
              });

            callback(null, result);
          });
      },
      function final(data, callback) {
        callback(null, {
          error: false,
          message: "Found",
          data: data
        });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

const getUser = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function getData(data, callback) {
        models.user
          .findAll({
              where: {
                  id: req.params.id
              }
          })
          .then((result, err) => {
            if (result.length < 1)
              return callback({
                error: true,
                message: "Not Found",
                data: {}
              });

            callback(null, result);
          });
      },
      function final(data, callback) {
        callback(null, {
          error: false,
          message: "Found",
          data: data
        });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

const updateUser = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function getData(data, callback) {
        models.user
          .update(
            {
              email: req.body.email,
              phone: req.body.phone,
              name: req.body.name
            },
            {
              where: {
                id: req.params.id
              }
            }
          )
          .then((result, err) => {
            callback(null, {
              error: false,
              message: "Success Update",
              data: data
            });
          });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

const deleteUser = (req, res) => {
  async.waterfall(
    [
      function validation(callback) {
        let valid = true;
        let dataValidator = [];

        req.validation.validate(dataValidator, err => {
          if (err) {
            valid = false;
            return req.output(req, res, err, "error", 400);
          }
        });

        if (!valid) return;
        callback(null, {});
      },
      function getData(data, callback) {
        models.user
          .destroy(
            {
              where: {
                id: req.params.id
              }
            }
          )
          .then((result, err) => {
            callback(null, {
              error: false,
              message: "Success Delete",
              data: data
            });
          });
      }
    ],
    (err, result) => {
      if (err) return req.output(req, res, err, "error", 400);
      return req.output(req, res, result, "info", 200);
    }
  );
};

module.exports = { createUser, listUser, getUser, updateUser, deleteUser }