const express = require("express");
const router = express.Router();
const jwt = require("../utils/jwt");
const authToken = jwt.authenticateToken;
const testController = require("../controller/testController");
const authController = require("../controller/authController");
const userController = require("../controller/userController");

module.exports = app => {
  router.get("/test", testController.list);

  // Auth
  router.post("/login", authController.login);

  // Admin
  router.post("/admin/user/create", authToken, userController.createUser);
  router.get("/admin/user", authToken, userController.listUser);
  router.post("/admin/user/update/:id", authToken, userController.updateUser);
  router.post("/admin/user/delete/:id", authToken, userController.deleteUser);

  // User
  router.get("/user/profile/:id", authToken, userController.getUser);

  app.use("/api", router);
};
